import numpy as np
from scipy.io import wavfile


def load_wav(wav_path):
    sr, audio = wavfile.read(wav_path)
    channel_num = 1 if len(audio.shape) == 1 else audio.shape[1]
    return audio, sr, channel_num


def split_channel(wav_path):
    audio, sr, channel_num = load_wav(wav_path)
    channels = None
    if channel_num == 1:
        channels = list(audio)
    else:
        channels = [[item[channel] for item in audio] for channel in range(channel_num)]
    return channels, sr


def get_channel(wav_path, channel, save_path=None):
    assert isinstance(channel, int) and channel >= 0
    audio, sr, channel_num = load_wav(wav_path)
    assert channel < channel_num, 'input channel not exists. channel num: {}'.format(channel_num)
    channel_data = None
    if channel_num == 1:
        channel_data = list(audio)
    else:
        channel_data = [item[channel] for item in audio]
    if save_path:
        wavfile.write(save_path, sr, np.array(channel_data))
    return channel_data, sr

