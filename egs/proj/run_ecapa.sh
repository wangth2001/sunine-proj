#!/bin/bash
# Copyright   2021   Tsinghua University (Author: Lantian Li, Yang Zhang)
# Apache 2.0.

SUNINE_ROOT=../..
vox1_wav_path=/ssd2/VoxCeleb/voxceleb1/voxceleb1_wav
vox2_path=/ssd2/VoxCeleb/voxceleb2
cnceleb1_path=/ssd2/CNC_v2.0/CN-Celeb
cnceleb2_path=/ssd2/CNC_v2.0/CN-Celeb2

training_data_path=/ssd2/HUAWEI_Robust_converted_data/train
eval_data_root=/home/wangtianhao/coproj/230329-HUAWEI-Robust/data_converted/eval
musan_path=/home/wangtianhao/coproj/230329-HUAWEI-Robust/data_converted/musan_8k
rirs_path=/home/wangtianhao/coproj/230329-HUAWEI-Robust/data_converted/RIRS_NOISES_8k

config=conf/ECAPA_ASP_AAMSoftmax-da_ms.yaml
exp_dir="exp/ecapa_asp_aam_da_ms"
ckpt_path=
dev_set=plda_train
cuda_device="7"  # change --gpus correspondingly if you change the device

stage=4
stop_stage=7


# compute VAD
if [ $stage -le 1 ] && [ $stop_stage -ge 1 ];then
  # compute VAD for training data
  echo Compute VAD on training data
  python3 $SUNINE_ROOT/steps/compute_vad.py \
          --data_dir $training_data_path/wav \
          --extension wav \
          --speaker_level 1 \
          --num_jobs 32
fi


if [ $stage -le 2 ] && [ $stop_stage -ge 2 ];then
  # prepare data

  echo "prepare training data ..."
  [ -d data/train ] && rm -r data/train
  mkdir -p data/train

  del_utt_shorter_than=5
  echo "del utts short than $del_utt_shorter_than s ..."
  for spk in $training_data_path/data/*; do
    find ${spk}/ -name "*.vad" | while read vad_file; do
      file_name=$(basename $vad_file)
      duration=$(soxi -D $vad_file)
      if [ $(echo "$duration < $del_utt_shorter_than" | bc) -eq 1 ]; then
        echo "$vad_file $duration" >> data/train/short_utts
        rm $vad_file
      else
        echo "$file_name $duration" >> data/train/utt2dur
      fi
    done
  done

  echo "generate vad.scp, utt2spk, spk2utt ..."
  for spk in $training_data_path/data/*; do
    find ${spk}/ -name "*.vad" | awk -F"/" '{print $NF,$0}' | sort >> data/train/vad.scp
    find ${spk}/ -name "*.vad" | awk -F"/" '{print $NF,$(NF-1)}' | sort >> data/train/utt2spk
  done

  local/utt2spk_to_spk2utt.pl data/train/utt2spk > data/train/spk2utt

  echo "Prepare eval data ..."
  local/prepare_eval_data.sh \
      --eval_data_root $eval_data_root \
      --vox1_wav_path $vox1_wav_path \
      --vox2_path $vox2_path \
      --cnceleb1_path $cnceleb1_path \
      --cnceleb2_path $cnceleb2_path \
      --stage 1 \
      --stop_stage 3

  # using for eval in training process
  [ -d data/eval ] && rm -r data/eval
  mkdir -p data/eval
  cat data/voxceleb/vox2_6000/enroll/wav.scp \
      data/voxceleb/vox2_6000/test/wav.scp \
      > data/eval/wav.scp
  cp data/voxceleb/vox2_6000/test/vox2_hw.trials data/eval/vox2_hw.trials

fi


if [ $stage -le 3 ] && [ $stop_stage -ge 3 ];then
  # prepare data for model training

  echo Build train list
  python3 $SUNINE_ROOT/steps/build_datalist.py \
          --data_dir $training_data_path/data \
          --extension vad \
          --speaker_level 1 \
          --data_list_path data/train_lst.csv

  echo Build $musan_path list
  python3 $SUNINE_ROOT/steps/build_datalist.py \
          --data_dir $musan_path \
          --extension wav \
          --data_list_path data/musan_lst.csv

  echo Build $rirs_path list
  python3 $SUNINE_ROOT/steps/build_datalist.py \
          --data_dir $rirs_path \
          --extension wav \
          --data_list_path data/rirs_lst.csv

  echo "Prepare dev data ..."
  local/prepare_plda_data.sh \
      --nj 32 \
      --plda_set $dev_set \
      --raw_data_num 20000 \
      --speed_perturb false
fi


if [ $stage -le 4 ] && [ $stop_stage -ge 4 ];then
  # model training
  echo "model training ..."

  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SUNINE_ROOT/main.py \
          --config $config \
          ${ckpt_path:+--checkpoint_path $ckpt_path} \
          --exp_dir $exp_dir \
          --train_list_path data/train_lst.csv \
          --musan_list_path data/musan_lst.csv \
          --rirs_list_path data/rirs_lst.csv \
          --eval_list_path data/eval/wav.scp \
          --trials_path data/eval/vox2_hw.trials \
          --distributed_backend dp \
          --reload_dataloaders_every_epoch \
          --gpus 1
fi


if [ $stage -le 5 ] && [ $stop_stage -ge 5 ];then
  # average checkpoints

  echo "average checkpoints ..."
  avg_model=$exp_dir/checkpoints/avg_model.ckpt
  last_n=10  # change conf.yaml correspondingly

  python $SUNINE_ROOT/steps/average_checkpoints.py \
      --src_path $exp_dir/checkpoints \
      --dest_model $avg_model \
      --last_n $last_n
fi


if [ $stage -le 6 ] && [ $stop_stage -ge 6 ];then
  # extract embedding
  ckpt_path=$exp_dir/checkpoints/avg_model.ckpt

  echo "extract embedding using pretrained model: $ckpt_path ..."

  for dset in voxceleb/vox1_enroll voxceleb/vox1_test; do
    echo "extract $dset ..."
    xvec_path=$exp_dir/embeddings/$dset
    [ -d $xvec_path ] && rm -r $xvec_path
    mkdir -p $xvec_path

    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SUNINE_ROOT/main.py \
            --config $config \
            ${ckpt_path:+--checkpoint_path $ckpt_path} \
            --evaluate \
            --extract \
            --exp_dir $exp_dir \
            --xvec_path $xvec_path \
            --eval_list_path data/$dset/wav.scp \
            --gpus 1
  done

  cat $exp_dir/embeddings/voxceleb/vox1_enroll/xvector.scp \
      $exp_dir/embeddings/voxceleb/vox1_test/xvector.scp \
      > $exp_dir/embeddings/voxceleb/vox1_test/eval_xvector.scp

  # # mean vector of enroll
  # echo "mean vector of enroll"
  # python $SUNINE_ROOT/steps/vector_mean.py \
  #   --spk2utt data/eval/enroll.map \
  #   --xvector_scp $exp_dir/embeddings/eval/xvector.scp \
  #   --spk_xvector_ark $exp_dir/embeddings/eval/enroll_spk_xvector.ark

  # cat $exp_dir/embeddings/eval/enroll_spk_xvector.scp >> $exp_dir/embeddings/eval/xvector.scp

  # # mean vector of test
  # echo "mean vector of test"
  # python $SUNINE_ROOT/steps/vector_mean.py \
  #   --spk2utt data/eval/test.map \
  #   --xvector_scp $exp_dir/embeddings/eval/xvector.scp \
  #   --spk_xvector_ark $exp_dir/embeddings/eval/test_spk_xvector.ark

  # cat $exp_dir/embeddings/eval/test_spk_xvector.scp >> $exp_dir/embeddings/eval/xvector.scp
fi


if [ $stage -le 7 ] && [ $stop_stage -ge 7 ];then
  # evaluation
  echo "cosine scoring ..."

  [ ! -d $exp_dir/scores ] && mkdir -p $exp_dir/scores

  for trials in vox1_cslt; do
    echo Evaluate $trials
  
    python -W ignore $SUNINE_ROOT/trainer/metric/compute_score.py \
            --trials_path data/eval/$trials.trials \
            --eval_scp_path $exp_dir/embeddings/voxceleb/vox1_test/eval_xvector.scp \
            --scores_path $exp_dir/scores/$trials.score
  
    python $SUNINE_ROOT/trainer/metric/compute_sid.py \
            --scores_file $exp_dir/scores/$trials.score \
            --topk "1,3,5"
  done

fi
