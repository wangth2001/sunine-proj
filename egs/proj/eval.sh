#!/bin/bash
# Copyright   2021   Tsinghua University (Author: Lantian Li, Yang Zhang)
# Apache 2.0.


SUNINE_ROOT=../..
vox1_wav_path=/ssd2/VoxCeleb/voxceleb1/voxceleb1_wav
vox2_path=/ssd2/VoxCeleb/voxceleb2
cnceleb1_path=/ssd2/CNC_v2.0/CN-Celeb
cnceleb2_path=/ssd2/CNC_v2.0/CN-Celeb2

training_data_path=/ssd2/HUAWEI_Robust_converted_data/train
eval_data_root=/home/wangtianhao/coproj/230329-HUAWEI-Robust/data_converted/eval
musan_path=/home/wangtianhao/coproj/230329-HUAWEI-Robust/data_converted/musan_8k
rirs_path=/home/wangtianhao/coproj/230329-HUAWEI-Robust/data_converted/RIRS_NOISES_8k

config=conf/ResNet34_TSP_AAMSoftmax.yaml
exp_dir="exp/resnet_tsp_aam"
ckpt_path=
dev_set=plda_train
cuda_device="0"  #change --gpus correspondingly if you change the device

stage=5
stop_stage=4


if [ $stage -le 5 ] && [ $stop_stage -ge 5 ];then
  echo "Prepare eval data ..."
  local/prepare_eval_data.sh \
      --eval_data_root $eval_data_root \
      --vox1_wav_path $vox1_wav_path \
      --vox2_path $vox2_path \
      --cnceleb1_path $cnceleb1_path \
      --cnceleb2_path $cnceleb2_path \
      --stage 1 \
      --stop_stage 2
fi


if [ $stage -le 6 ] && [ $stop_stage -ge 6 ];then
  # extract embedding
  ckpt_path=$exp_dir/checkpoints/avg_model.ckpt
  echo "extract embedding using pretrained model: $ckpt_path ..."

  local/extract_xvectors.sh \
      --SUNINE_ROOT $SUNINE_ROOT \
      --conf $config \
      --exp_dir $exp_dir \
      --ckpt_path $ckpt_path \
      --dev_set $dev_set/augment \
      --cuda_device $cuda_device \
      --stage 1 \
      --stop_stage 4
fi


if [ $stage -le 7 ] && [ $stop_stage -ge 7 ];then
  # evaluation
  echo "cosine scoring ..."
  [ ! -d $exp_dir/scores ] && mkdir -p $exp_dir/scores

  local/kaldi_eval.sh \
      --exp_dir $exp_dir \
      --lda_dim 128 \
      --dev_set $dev_set/augment \
      --stage 1 \
      --stop_stage 3
fi
