import os
import fire
import random
import numpy as np
import pandas as pd
import sox
from tqdm import tqdm
from scipy import signal
from scipy.io import wavfile


def get_chunk(audio, sr):
    chunk_size = sr * 300
    if audio.shape[0] < chunk_size:
        num_repeats = chunk_size // audio.shape[0] + 1
        audio = np.tile(audio, num_repeats)[:chunk_size]
    elif audio.shape[0] > chunk_size:
        start_frame = random.randint(0, audio.shape[0] - chunk_size)
        audio = audio[start_frame : start_frame + chunk_size]
    return audio.astype(np.float32)


def speed_perturb(waveform, sample_rate, speed_idx):
    speeds = [1.0, 0.9, 1.1]
    suffix = ''
    if speed_idx > 0:
        tfm = sox.Transformer()
        tfm.speed(speeds[speed_idx])
        tfm.rate(sample_rate)
        waveform = tfm.build_array(input_array=waveform, sample_rate_in=sample_rate)
        suffix = '-sp' + str(speeds[speed_idx]).replace('.', '')
    return waveform, suffix


class Augment:
    def __init__(self, musan_csv, rirs_csv):
        self.noise_snr = {'noise':[0, 15], 'speech':[13, 20], 'music':[5, 15]}
        self.num_noise = {'noise':[1, 1], 'speech':[3, 7], 'music':[1, 1]}
        self.musan_list = {'noise': [], 'speech': [], 'music': []}
        df = pd.read_csv(musan_csv)
        musan_files = df["utt_paths"].values
        musan_types = df["speaker_name"].values
        for idx, data in enumerate(musan_files):
            self.musan_list[musan_types[idx]].append(data)
        df = pd.read_csv(rirs_csv)
        self.rirs_list = df["utt_paths"].values

    def additive_noise(self, noise_type, audio):
        clean_db = 10 * np.log10(np.mean(audio ** 2) + 1e-4)
        num_noise = self.num_noise[noise_type]
        noise_list = random.sample(self.musan_list[noise_type], random.randint(num_noise[0], num_noise[1]))
        noises = []
        noise_log = []
        for noise_path in noise_list:
            sr, noise = wavfile.read(noise_path)
            noise = noise.astype(np.float32)
            noise = get_chunk(noise, sr)
            noise_snr = random.uniform(self.noise_snr[noise_type][0], self.noise_snr[noise_type][1])
            noise_db = 10 * np.log10(np.mean(noise ** 2) + 1e-4)
            noise = np.sqrt(10 ** ((clean_db - noise_db - noise_snr) / 10)) * noise
            noises.append(noise)
            noise_log.append([noise_path, noise_snr])
        audio = np.sum(noises, axis=0) + audio
        return audio, noise_log

    def reverberate(self, audio):
        audio_len = audio.shape[0]
        while True:
            try:
                rir_file = random.choice(self.rirs_list)
                sr, rir = wavfile.read(rir_file)
                rir = rir.astype(np.float32)
                rir = rir / np.sqrt(np.sum(rir ** 2))
                audio = signal.convolve(audio, rir, mode='full')[:audio_len]
                noise_log = [[rir_file, 'reverb']]
                return audio, noise_log
            except:
                continue


def data_aug(augment: Augment, audio, spk, utt):
    noise_types = ['noise', 'speech', 'music']
    aug_audios = [(spk, utt, audio)]
    noise_logs = [[]]
    for noise_type in noise_types:
        aug_audio, noise_log = augment.additive_noise(noise_type, audio)
        aug_utt = '{}-{}'.format(utt, noise_type)
        aug_audios.append((spk, aug_utt, aug_audio))
        noise_logs.append(noise_log)
    aug_audio, noise_log = augment.reverberate(audio)
    aug_utt = '{}-{}'.format(utt, 'reverb')
    aug_audios.append((spk, aug_utt, aug_audio))
    noise_logs.append(noise_log)
    return aug_audios, noise_logs


def main_split(raw_wav_scp_path, raw_utt2spk_path, musan_csv, rirs_csv, wav_save_path, 
               aug_wav_scp_path, aug_utt2spk_path, apply_speed_perturb=False, seed=0, aug_log_path=os.devnull):
    apply_speed_perturb = True if apply_speed_perturb == 'true' else False
    print('apply_speed_perturb:', apply_speed_perturb)
    print('seed:', seed)
    random.seed(seed)
    wav_list = []
    with open(raw_wav_scp_path, 'r') as raw_wav_scp:
        for line in raw_wav_scp.readlines():
            line = line.strip().split()
            wav_list.append(line)

    utt2spk = {}
    with open(raw_utt2spk_path, 'r') as raw_utt2spk:
        for line in raw_utt2spk.readlines():
            line = line.strip().split()
            utt2spk[line[0]] = line[1]

    augment = Augment(musan_csv, rirs_csv)

    with open(aug_wav_scp_path, 'w') as aug_wav_scp, open(aug_utt2spk_path, 'w') as aug_utt2spk, \
         open(aug_log_path, 'w') as aug_log:
        for item in tqdm(wav_list):
            sr, audio = wavfile.read(item[1])
            aug_data = []
            aug_logs = []
            if apply_speed_perturb:
                for speed_idx in [0, 1, 2]:
                    waveform, suffix = speed_perturb(audio, sr, speed_idx)
                    waveform = get_chunk(waveform, sr)
                    spk = utt2spk[item[0]] + suffix
                    utt = item[0] + suffix
                    aug_audios, noise_logs = data_aug(augment, waveform, spk, utt)
                    aug_data += aug_audios
                    aug_logs += noise_logs
            else:
                waveform = get_chunk(audio, sr)
                aug_audios, noise_logs = data_aug(augment, waveform, utt2spk[item[0]], item[0])
                aug_data += aug_audios
                aug_logs += noise_logs
            
            for item in zip(aug_data, aug_logs):
                spk, utt, audio = item[0]
                spk_save_path = os.path.join(wav_save_path, spk)
                os.makedirs(spk_save_path, exist_ok=True)
                utt_save_path = os.path.join(spk_save_path, utt + '.wav')
                wavfile.write(utt_save_path, sr, audio.astype(np.int16))
                aug_wav_scp.write('{} {}\n'.format(utt, utt_save_path))
                aug_utt2spk.write('{} {}\n'.format(utt, spk))
                for noise in item[1]:
                    aug_log.write('{} {} {}\n'.format(utt, noise[0], noise[1]))
                    utt = ' ' * len(utt)


if __name__ == '__main__':
    fire.Fire(main_split)
