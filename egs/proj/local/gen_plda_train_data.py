import os
import fire


def main(utt2dur_path, vad_scp_path, utt2spk_path, dest_data_path):
    uttkey2path = {}
    with open(vad_scp_path, 'r') as vad_scp:
        for line in vad_scp.readlines():
            line = line.strip().split()
            uttkey2path[line[0]] = line[1]
    
    utt2spk = {}
    with open(utt2spk_path, 'r') as utt2spk_f:
        for line in utt2spk_f.readlines():
            line = line.strip().split()
            utt2spk[line[0]] = line[1]
    
    dest_wav_scp = os.path.join(dest_data_path, 'wav.scp')
    dest_utt2spk = os.path.join(dest_data_path, 'utt2spk')

    with open(utt2dur_path, 'r') as utt2dur, \
         open(dest_wav_scp, 'w') as wav_scp_f, open(dest_utt2spk, 'w') as utt2spk_f:
        for line in utt2dur.readlines():
            line = line.strip().split()
            wav_scp_f.write('{} {}\n'.format(line[0].replace('.vad', ''), uttkey2path[line[0]]))
            utt2spk_f.write('{} {}\n'.format(line[0].replace('.vad', ''), utt2spk[line[0]]))


if __name__ == '__main__':
    fire.Fire(main)
