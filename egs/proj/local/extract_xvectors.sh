#!/bin/bash

SUNINE_ROOT=../..
conf=conf/config.yaml
exp_dir=exp
ckpt_path=$exp_dir/checkpoints/avg_model.ckpt
dev_set=plda_train
cuda_device="0"

stage=1
stop_stage=4

. ./local/parse_options.sh || exit 1;
set -e


if [ $stage -le 1 ] && [ $stop_stage -ge 1 ]; then
  echo "extract plda training xvectors ..."
  echo "plda set: $dev_set"

  xvec_path=$exp_dir/embeddings/$dev_set
  [ -d $xvec_path ] && rm -r $xvec_path
  mkdir -p $xvec_path
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SUNINE_ROOT/main.py \
          --config $conf \
          ${ckpt_path:+--checkpoint_path $ckpt_path} \
          --evaluate \
          --extract \
          --exp_dir $exp_dir \
          --xvec_path $xvec_path \
          --eval_list_path data/$dev_set/wav.scp \
          --gpus 1
fi


if [ $stage -le 2 ] && [ $stop_stage -ge 2 ]; then
  echo "extract voxceleb xvectors ..."

  for dset in voxceleb/vox1-1000 voxceleb/vox2-6000; do
    echo "extract $dset ..."
    xvec_path=$exp_dir/embeddings/$dset
    [ -d $xvec_path ] && rm -r $xvec_path
    mkdir -p $xvec_path

    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SUNINE_ROOT/main.py \
            --config $conf \
            ${ckpt_path:+--checkpoint_path $ckpt_path} \
            --evaluate \
            --extract \
            --exp_dir $exp_dir \
            --xvec_path $xvec_path \
            --eval_list_path data/$dset/wav.scp \
            --gpus 1

    echo "mean vector of $dset ..."
    python $SUNINE_ROOT/steps/vector_mean.py \
      --spk2utt data/$dset/enroll.map \
      --xvector_scp $xvec_path/xvector.scp \
      --spk_xvector_ark $xvec_path/enroll_spk_xvector.ark
    
    python $SUNINE_ROOT/steps/vector_mean.py \
      --spk2utt data/$dset/test.map \
      --xvector_scp $xvec_path/xvector.scp \
      --spk_xvector_ark $xvec_path/test_spk_xvector.ark
    
    cat $xvec_path/enroll_spk_xvector.scp \
        $xvec_path/test_spk_xvector.scp \
        > $xvec_path/spk_xvector.scp
  done
fi


if [ $stage -le 4 ] && [ $stop_stage -ge 4 ]; then
  echo "extract cnceleb xvectors ..."
  cnceleb=("entertainment" "interview" "live_broadcast" "speech")

  for enroll in "${cnceleb[@]}"; do

    echo "extract $enroll enroll ..."
    enroll_wav_scp_path=data/cnceleb/$enroll/enroll_$enroll
    enroll_xvec_path=$exp_dir/embeddings/cnceleb/$enroll/enroll_$enroll

    [ -d $enroll_xvec_path ] && rm -r $enroll_xvec_path
    mkdir -p $enroll_xvec_path

    CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SUNINE_ROOT/main.py \
            --config $conf \
            ${ckpt_path:+--checkpoint_path $ckpt_path} \
            --evaluate \
            --extract \
            --exp_dir $exp_dir \
            --xvec_path $enroll_xvec_path \
            --eval_list_path $enroll_wav_scp_path/wav.scp \
            --gpus 1
    
    echo "mean vector of $enroll enroll ..."
    python $SUNINE_ROOT/steps/vector_mean.py \
      --spk2utt $enroll_wav_scp_path/enroll.map \
      --xvector_scp $enroll_xvec_path/xvector.scp \
      --spk_xvector_ark $enroll_xvec_path/enroll_spk_xvector.ark

    for test in "${cnceleb[@]}"; do
      echo "extract $enroll-$test test ..."

      test_wav_scp_path=data/cnceleb/$enroll/test_$test
      test_xvec_path=$exp_dir/embeddings/cnceleb/$enroll/test_$test

      [ -d $test_xvec_path ] && rm -r $test_xvec_path
      mkdir -p $test_xvec_path

      CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SUNINE_ROOT/main.py \
              --config $conf \
              ${ckpt_path:+--checkpoint_path $ckpt_path} \
              --evaluate \
              --extract \
              --exp_dir $exp_dir \
              --xvec_path $test_xvec_path \
              --eval_list_path $test_wav_scp_path/wav.scp \
              --gpus 1
      
      echo "mean vector of $enroll-$test test ..."
      python $SUNINE_ROOT/steps/vector_mean.py \
        --spk2utt $test_wav_scp_path/test.map \
        --xvector_scp $test_xvec_path/xvector.scp \
        --spk_xvector_ark $test_xvec_path/test_spk_xvector.ark
      
      cat $enroll_xvec_path/xvector.scp \
          $test_xvec_path/xvector.scp \
          > $test_xvec_path/concat_xvector.scp
      
      cat $enroll_xvec_path/enroll_spk_xvector.scp \
          $test_xvec_path/test_spk_xvector.scp \
          > $test_xvec_path/spk_xvector.scp
    done
  done
fi
