#!/bin/bash

SUNINE_ROOT=../..
exp_dir=exp

stage=1
stop_stage=3

. ./local/parse_options.sh || exit 1;


if [ $stage -le 1 ] && [ $stop_stage -ge 1 ]; then
  echo "Evaluate VoxCeleb1 ..."

  test_data_dir=data/voxceleb/vox1-1000
  test_xvec_dir=$exp_dir/embeddings/voxceleb/vox1-1000

  python -W ignore $SUNINE_ROOT/trainer/metric/compute_score.py \
          --trials_path $test_data_dir/vox1_hw.trials \
          --eval_scp_path $test_xvec_dir/xvector.scp \
          --scores_path $exp_dir/scores/vox1_hw.score

  python $SUNINE_ROOT/trainer/metric/compute_sid.py \
          --scores_file $exp_dir/scores/vox1_hw.score \
          --topk "1,3,5"

  echo "done"
fi


if [ $stage -le 2 ] && [ $stop_stage -ge 2 ]; then
  echo "Evaluate VoxCeleb2 ..."

  test_data_dir=data/voxceleb/vox2-6000
  test_xvec_dir=$exp_dir/embeddings/voxceleb/vox2-6000

  python -W ignore $SUNINE_ROOT/trainer/metric/compute_score.py \
          --trials_path $test_data_dir/vox2_hw.trials \
          --eval_scp_path $test_xvec_dir/xvector.scp \
          --scores_path $exp_dir/scores/vox2_hw.score

  python $SUNINE_ROOT/trainer/metric/compute_sid.py \
          --scores_file $exp_dir/scores/vox2_hw.score \
          --topk "1,3,5"

  echo "done"
fi


if [ $stage -le 3 ] && [ $stop_stage -ge 3 ]; then
  cnceleb=("entertainment" "interview" "live_broadcast" "speech")

  echo "Evaluate CN-Celeb ..."
  for enroll in "${cnceleb[@]}"; do
    for test in "${cnceleb[@]}"; do
      echo ""
      echo ">>> $enroll $test"
      
      test_data_dir=data/cnceleb/$enroll/test_$test
      test_xvec_dir=$exp_dir/embeddings/cnceleb/$enroll/test_$test

      [ ! -d $exp_dir/scores ] && mkdir -p $exp_dir/scores

      echo ""
      echo "Utters concat scoring ..."
      python -W ignore $SUNINE_ROOT/trainer/metric/compute_score.py \
              --trials_path $test_data_dir/cnc_${enroll}_${test}.trials \
              --eval_scp_path $test_xvec_dir/concat_xvector.scp \
              --scores_path $exp_dir/scores/cnc_${enroll}_${test}_concat.score

      python $SUNINE_ROOT/trainer/metric/compute_sid.py \
              --scores_file $exp_dir/scores/cnc_${enroll}_${test}_concat.score \
              --topk "1,5,10"

      echo ""
      echo "Embedding average scoring ..."
      python -W ignore $SUNINE_ROOT/trainer/metric/compute_score.py \
              --trials_path $test_data_dir/cnc_${enroll}_${test}.trials \
              --eval_scp_path $test_xvec_dir/spk_xvector.scp \
              --scores_path $exp_dir/scores/cnc_${enroll}_${test}_avg.score

      python $SUNINE_ROOT/trainer/metric/compute_sid.py \
              --scores_file $exp_dir/scores/cnc_${enroll}_${test}_avg.score \
              --topk "1,5,10"

    done
  done
  echo "done"
fi
