import os
import fire


def main(enroll_spk2utt, test_utt2spk, dest_trials):
    print('enroll:', enroll_spk2utt)
    print('test:', test_utt2spk)
    print('output trials:', dest_trials)

    enroll_spks = []
    with open(enroll_spk2utt, 'r') as f:
        for line in f.readlines():
            line = line.strip().split()
            enroll_spks.append(line[0])

    test_utts = []
    utt2spk = {}
    with open(test_utt2spk, 'r') as f:
        for line in f.readlines():
            line = line.strip().split()
            test_utts.append(line[0])
            utt2spk[line[0]] = line[1]

    with open(dest_trials, 'w') as trials:
        for enroll_spk in enroll_spks:
            for test in test_utts:
                label = 'target' if enroll_spk == utt2spk[test] else 'nontarget'
                trials.write('{} {} {}\n'.format(enroll_spk, test, label))

    print('make kaldi trials OK. trial num:', len(enroll_spks) * len(test_utts))


if __name__ == '__main__':
    fire.Fire(main)
