import fire


def main(enroll_utt2spk, test_utt2spk, dest_trials):
    print('enroll:', enroll_utt2spk)
    print('test:', test_utt2spk)
    print('output trials:', dest_trials)

    utt2spk = {}
    enroll_utts = []
    with open(enroll_utt2spk, 'r') as f:
        for line in f.readlines():
            line = line.strip().split()
            enroll_utts.append(line[0])
            utt2spk[line[0]] = line[1]

    test_utts = []
    with open(test_utt2spk, 'r') as f:
        for line in f.readlines():
            line = line.strip().split()
            test_utts.append(line[0])
            utt2spk[line[0]] = line[1]

    with open(dest_trials, 'w') as trials:
        for enroll in enroll_utts:
            for test in test_utts:
                label = '1' if utt2spk[enroll] == utt2spk[test] else '0'
                trials.write('{} {} {}\n'.format(label, enroll, test))

    print('make trials OK. trial num:', len(enroll_utts) * len(test_utts))


if __name__ == '__main__':
    fire.Fire(main)
