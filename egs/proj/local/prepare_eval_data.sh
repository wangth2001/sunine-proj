#!/bin/bash

eval_data_root=/coproj/HUAWEI-Robust/data_converted/eval
vox1_wav_path=/data/VoxCeleb/voxceleb1/voxceleb1_wav
vox2_path=/data/VoxCeleb/voxceleb2
cnceleb1_path=/data/CNC_v2.0/CN-Celeb
cnceleb2_path=/data/CNC_v2.0/CN-Celeb2

stage=1
stop_stage=3

. ./local/parse_options.sh || exit 1;
set -e


if [ $stage -le 1 ] && [ $stop_stage -ge 1 ];then
  echo "Prepare voxceleb1 eval data ..."

  # generate vox1 wav.scp, utt2spk, spk2utt, spk2num ...
  [ -d data/voxceleb/vox1-1000 ] && rm -r data/voxceleb/vox1-1000
  mkdir -p data/voxceleb/vox1-1000

  cat $eval_data_root/vox-metadata/vox1-1000/enroll_voxceleb1 | sort > data/voxceleb/vox1-1000/enroll.map
  awk '{print $1,$1}' data/voxceleb/vox1-1000/enroll.map > data/voxceleb/vox1-1000/enroll_utt2spk
  awk '{print $0}' data/voxceleb/vox1-1000/enroll.map | \
  awk -v p=$vox1_wav_path '{for(i=2;i<=NF;i++){print $i, p"/"$i}}' > data/voxceleb/vox1-1000/wav.scp

  cat $eval_data_root/vox-metadata/vox1-1000/test_voxceleb1 | sort > data/voxceleb/vox1-1000/test.lst
  awk '{print $0}' data/voxceleb/vox1-1000/test.lst | \
  awk -v p=$vox1_wav_path '{for(i=2;i<=NF;i++){print $i, p"/"$i}}' >> data/voxceleb/vox1-1000/wav.scp
  awk '{print $0}' data/voxceleb/vox1-1000/test.lst | \
  awk '{for(i=2;i<=NF;i++){print $i, $1}}' >> data/voxceleb/vox1-1000/test_utt2spk

  # generate vox1 trials ...
  python local/make_trials.py \
      --enroll_utt2spk data/voxceleb/vox1-1000/enroll_utt2spk \
      --test_utt2spk data/voxceleb/vox1-1000/test_utt2spk \
      --dest_trials data/voxceleb/vox1-1000/vox1_hw.trials

  echo "Prepare voxceleb1 eval data OK."
fi


if [ $stage -le 2 ] && [ $stop_stage -ge 2 ];then
  echo "Prepare voxceleb2 eval data ..."

  # make voxceleb2 links
  voxceleb2_links=$eval_data_root/voxceleb2_links
  [ -d $voxceleb2_links ] && rm -r $voxceleb2_links
  mkdir -p $voxceleb2_links

  for spk in `ls $vox2_path/dev/aac`; do
      ln -s $vox2_path/dev/aac/$spk $voxceleb2_links/$spk
  done

  for spk in `ls $vox2_path/test/aac`; do
      ln -s $vox2_path/test/aac/$spk $voxceleb2_links/$spk
  done

  # generate vox2 wav.scp, utt2spk, spk2utt, spk2num ...
  [ -d data/voxceleb/vox2-6000 ] && rm -r data/voxceleb/vox2-6000
  mkdir -p data/voxceleb/vox2-6000

  cat $eval_data_root/vox-metadata/vox2-6000/enroll_voxceleb | sort > data/voxceleb/vox2-6000/enroll.map
  awk '{print $1,$1}' data/voxceleb/vox2-6000/enroll.map > data/voxceleb/vox2-6000/enroll_utt2spk
  awk '{print $0}' data/voxceleb/vox2-6000/enroll.map | \
  awk -v p=$vox2_path '{for(i=2;i<=NF;i++){print $i, p"/"$i}}' > data/voxceleb/vox2-6000/wav.scp

  cat $eval_data_root/vox-metadata/vox2-6000/test_voxceleb | sort > data/voxceleb/vox2-6000/test.lst
  awk '{print $0}' data/voxceleb/vox2-6000/test.lst | \
  awk -v p=$vox2_path '{for(i=2;i<=NF;i++){print $i, p"/"$i}}' >> data/voxceleb/vox2-6000/wav.scp
  awk '{print $0}' data/voxceleb/vox2-6000/test.lst | \
  awk '{for(i=2;i<=NF;i++){print $i, $1}}' >> data/voxceleb/vox2-6000/test_utt2spk

  # generate vox2 trials ...
  python local/make_trials.py \
      --enroll_utt2spk data/voxceleb/vox2-6000/enroll_utt2spk \
      --test_utt2spk data/voxceleb/vox2-6000/test_utt2spk \
      --dest_trials data/voxceleb/vox2-6000/vox2_hw.trials

  echo "Prepare voxceleb2 eval data OK."
fi


if [ $stage -le 3 ] && [ $stop_stage -ge 3 ];then
  cnceleb=("entertainment" "interview" "live_broadcast" "speech")

  echo "Prepare cnceleb eval data ..."

  # make cnceleb1 and cnceleb2 links
  cnceleb_links=$eval_data_root/cnceleb_links
  [ -d $cnceleb_links ] && rm -r $cnceleb_links
  mkdir -p $cnceleb_links

  for spk in `ls $cnceleb1_path/data`; do
      ln -s $cnceleb1_path/data/$spk $cnceleb_links/$spk
  done

  for spk in `ls $cnceleb2_path/data`; do
      ln -s $cnceleb2_path/data/$spk $cnceleb_links/$spk
  done

  # concat cnceleb {enroll, test} utterances according to {enroll.map, test.map}
  # for dest_path in $eval_data_root/cnceleb/*/*; do
  #     echo "concating $dest_path"
  #     python local/wav_convert_tools/concat_eval.py \
  #         --data_root $cnceleb_links \
  #         --dest_path $dest_path \
  #         > ${dest_path}/concat.log 2>&1 &
  # done
  # wait

  for enroll in "${cnceleb[@]}"; do
    echo "Prepare $enroll enroll data ..."

    # generate cnceleb enroll wav.scp, utt2spk, spk2utt ...
    [ -d data/cnceleb/$enroll/enroll_$enroll ] && rm -r data/cnceleb/$enroll/enroll_$enroll
    mkdir -p data/cnceleb/$enroll/enroll_$enroll

    # cat $eval_data_root/cnceleb/$enroll/enroll_$enroll/wav_concat.scp | sort > data/cnceleb/$enroll/enroll_$enroll/wav.scp
    # cat $eval_data_root/cnceleb/$enroll/enroll_$enroll/utt2spk        | sort > data/cnceleb/$enroll/enroll_$enroll/utt2spk
    # cat $eval_data_root/cnceleb/$enroll/enroll_$enroll/spk2utt        | sort > data/cnceleb/$enroll/enroll_$enroll/spk2utt
    # awk '{print $1, NF-1}' data/cnceleb/$enroll/enroll_$enroll/spk2utt       > data/cnceleb/$enroll/enroll_$enroll/spk2num

    cat $eval_data_root/cnceleb/$enroll/enroll_$enroll/enroll.map | sort > data/cnceleb/$enroll/enroll_$enroll/enroll.map
    awk '{print $0}' data/cnceleb/$enroll/enroll_$enroll/enroll.map | \
    awk -v p=$cnceleb1_path '{for(i=2;i<=NF;i++){print $i, p"/"$i}}' > data/cnceleb/$enroll/enroll_$enroll/enroll.scp
    cat data/cnceleb/$enroll/enroll_$enroll/enroll.scp >> data/cnceleb/$enroll/enroll_$enroll/wav.scp

    for test in "${cnceleb[@]}"; do
      echo "Prepare $enroll-$test test data ..."

      # generate cnceleb test wav.scp, utt2spk, spk2utt ...
      # [ -d data/cnceleb/$enroll/test_$test ] && rm -r data/cnceleb/$enroll/test_$test
      # mkdir -p data/cnceleb/$enroll/test_$test

      # cat $eval_data_root/cnceleb/$enroll/test_$test/wav_concat.scp | sort > data/cnceleb/$enroll/test_$test/wav.scp
      # cat $eval_data_root/cnceleb/$enroll/test_$test/utt2spk        | sort > data/cnceleb/$enroll/test_$test/utt2spk
      # cat $eval_data_root/cnceleb/$enroll/test_$test/spk2utt        | sort > data/cnceleb/$enroll/test_$test/spk2utt

      cat $eval_data_root/cnceleb/$enroll/test_$test/test.map | sort > data/cnceleb/$enroll/test_$test/test.map
      awk '{print $0}' data/cnceleb/$enroll/test_$test/test.map | \
      awk -v p=$cnceleb1_path '{for(i=2;i<=NF;i++){print $i, p"/"$i}}' > data/cnceleb/$enroll/test_$test/test.scp
      cat data/cnceleb/$enroll/test_$test/test.scp >> data/cnceleb/$enroll/test_$test/wav.scp

      # generate cnceleb trials ...
      python local/make_trials.py \
          --enroll_utt2spk data/cnceleb/$enroll/enroll_$enroll/utt2spk \
          --test_utt2spk data/cnceleb/$enroll/test_$test/utt2spk \
          --dest_trials data/cnceleb/$enroll/test_$test/cnc_${enroll}_${test}.trials
      
    done
  done
  echo "Prepare cnceleb eval data OK."
fi
