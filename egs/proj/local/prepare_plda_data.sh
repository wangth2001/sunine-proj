#!/bin/bash

nj=32
plda_set=plda_train
raw_data_num=20000
speed_perturb=false  # true or false

. ./local/parse_options.sh || exit 1;
set -e

plda_train_path=data/$plda_set

stage=1
stop_stage=1


# prepare PLDA training data
if [ $stage -le 1 ] && [ $stop_stage -ge 1 ]; then
  [ -d $plda_train_path ] && rm -r $plda_train_path
  mkdir -p $plda_train_path/raw

  echo "choose longer utts from training data ..."
  sort -n -k 2 data/train/utt2dur | tail -n $raw_data_num | sort > $plda_train_path/raw/utt2dur_20k
  python local/gen_plda_train_data.py \
      --utt2dur_path $plda_train_path/raw/utt2dur_20k \
      --vad_scp_path data/train/vad.scp \
      --utt2spk_path data/train/utt2spk \
      --dest_data_path $plda_train_path/raw


  echo "apply data augmentation to PLDA training data ..."
  raw_data_path=$plda_train_path/raw
  aug_data_path=$plda_train_path/augment
  mkdir -p $aug_data_path/wav

  data_list=$raw_data_path/wav.scp
  raw_log_dir=$raw_data_path/split_log
  mkdir -p $raw_log_dir
  aug_log_dir=$aug_data_path/split_log
  mkdir -p $aug_log_dir

  data_num=$(wc -l ${data_list} | awk '{print $1}')
  subfile_num=$(($data_num / $nj + 1))
  split -l ${subfile_num} -d -a 3 ${data_list} ${raw_log_dir}/split_

  for job in $(seq 0 $(($nj - 1))); do
    suffix=$(printf '%03d' $job)
    data_list_subfile=${raw_log_dir}/split_${suffix}
    wav_scp_split=${aug_log_dir}/wav.scp_split_${suffix}
    utt2spk_split=${aug_log_dir}/utt2spk_split_${suffix}
    aug_log_split=${aug_log_dir}/aug_log_split_${suffix}

    python -W ignore local/augment_data.py \
        --raw_wav_scp_path $data_list_subfile \
        --raw_utt2spk_path $raw_data_path/utt2spk \
        --musan_csv data/musan_lst.csv \
        --rirs_csv data/rirs_lst.csv \
        --wav_save_path $aug_data_path/wav \
        --aug_wav_scp_path $wav_scp_split \
        --aug_utt2spk_path $utt2spk_split \
        --apply_speed_perturb $speed_perturb \
        --seed $job \
        --aug_log_path $aug_log_split \
       >${aug_log_dir}/split_${suffix}.log 2>&1 &
  done
  wait

  cat $aug_log_dir/wav.scp_split_* > $aug_data_path/wav.scp
  cat $aug_log_dir/utt2spk_split_* > $aug_data_path/utt2spk
  cat $aug_log_dir/aug_log_split_* > $aug_data_path/augment.log

  local/utt2spk_to_spk2utt.pl $aug_data_path/utt2spk > $aug_data_path/spk2utt
  awk '{print $1, NF-1}' $aug_data_path/spk2utt > $aug_data_path/spk2num

  aug_num=$(wc -l $aug_data_path/wav.scp | awk '{print $1}')
  echo "augment done"
  echo "raw wav.scp num: $data_num"
  echo "aug wav.scp num: $aug_num (contain raw)"

fi
