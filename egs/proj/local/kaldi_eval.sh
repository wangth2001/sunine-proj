#!/bin/bash

exp_dir=exp
lda_dim=128
dev_set=plda_train/augment

stage=1
stop_stage=3

. ./path.sh
. ./local/parse_options.sh || exit 1;
export PATH=$CONDA_PREFIX/bin:$PATH
set -e

embed_dir=$exp_dir/embeddings
dev_data_dir=data/$dev_set
dev_xvec_dir=$embed_dir/$dev_set


if [ $stage -le 1 ] && [ $stop_stage -ge 1 ]; then
  echo "Evaluate VoxCeleb1 ..."

  enroll_data_dir=data/voxceleb/vox1_1000/enroll
  enroll_xvec_dir=$embed_dir/voxceleb/vox1_1000/enroll
  test_data_dir=data/voxceleb/vox1_1000/test
  test_xvec_dir=$embed_dir/voxceleb/vox1_1000/test
  trials=$test_data_dir/vox1_hw.trials.kaldi
  scores_dir=$exp_dir/scores/vox1_scores

  local/lda_plda_scoring.sh --use_existing_models false --lda-dim $lda_dim --covar-factor 0.0 \
                            $dev_data_dir $enroll_data_dir $test_data_dir \
                            $dev_xvec_dir $enroll_xvec_dir $test_xvec_dir \
                            $trials $scores_dir

  eer=$(paste $trials $scores_dir/lda_plda_scores | awk '{print $6, $3}' | compute-eer - 2>/dev/null)
  echo "LDA_PLDA EER: $eer%"

  for score in lda_plda_scores; do
    echo "Compute $score SID Top-K ACC ..."
    python local/compute_sid_kaldi.py \
        --scores_file $scores_dir/$score \
        --utt2spk_file $test_data_dir/utt2spk \
        --topk "1,3,5"
  done
  echo "done"
fi


if [ $stage -le 2 ] && [ $stop_stage -ge 2 ]; then
  echo "Evaluate VoxCeleb2 ..."

  enroll_data_dir=data/voxceleb/vox2_6000/enroll
  enroll_xvec_dir=$embed_dir/voxceleb/vox2_6000/enroll
  test_data_dir=data/voxceleb/vox2_6000/test
  test_xvec_dir=$embed_dir/voxceleb/vox2_6000/test
  trials=$test_data_dir/vox2_hw.trials.kaldi
  scores_dir=$exp_dir/scores/vox2_scores

  local/lda_plda_scoring.sh --use_existing_models false --lda-dim $lda_dim --covar-factor 0.0 \
                            $dev_data_dir $enroll_data_dir $test_data_dir \
                            $dev_xvec_dir $enroll_xvec_dir $test_xvec_dir \
                            $trials $scores_dir

  eer=$(paste $trials $scores_dir/lda_plda_scores | awk '{print $6, $3}' | compute-eer - 2>/dev/null)
  echo "LDA_PLDA EER: $eer%"

  for score in lda_plda_scores; do
    echo "Compute $score SID Top-K ACC ..."
    python local/compute_sid_kaldi.py \
        --scores_file $scores_dir/$score \
        --utt2spk_file $test_data_dir/utt2spk \
        --topk "1,3,5"
  done
  echo "done"
fi


if [ $stage -le 3 ] && [ $stop_stage -ge 3 ]; then
  cnceleb=("entertainment" "interview" "live_broadcast" "speech")
  echo "Evaluate CN-Celeb ..."
  for enroll in "${cnceleb[@]}"; do
    for test in "${cnceleb[@]}"; do
      echo ""
      echo ">>> $enroll $test"

      enroll_data_dir=data/cnceleb/$enroll/enroll_$enroll
      enroll_xvec_dir=$embed_dir/cnceleb/$enroll/enroll_$enroll
      test_data_dir=data/cnceleb/$enroll/test_$test
      test_xvec_dir=$embed_dir/cnceleb/$enroll/test_$test
      trials=$test_data_dir/cnc_${enroll}_${test}.trials.kaldi
      scores_dir=$exp_dir/scores/cnceleb/$enroll/${test}_scores

      local/lda_plda_scoring.sh --use_existing_models false --lda-dim $lda_dim --covar-factor 0.0 \
                                $dev_data_dir $enroll_data_dir $test_data_dir \
                                $dev_xvec_dir $enroll_xvec_dir $test_xvec_dir \
                                $trials $scores_dir

      eer=$(paste $trials $scores_dir/lda_plda_scores | awk '{print $6, $3}' | compute-eer - 2>/dev/null)
      echo "LDA_PLDA EER: $eer%"

      for score in lda_plda_scores; do
          echo "Compute $score SID Top-K ACC ..."
          python local/compute_sid_kaldi.py \
              --scores_file $scores_dir/$score \
              --utt2spk_file $test_data_dir/utt2spk \
              --topk "1,5,10"
      done
    done
  done
  echo "done"
fi
