import os
import fire
from tqdm import tqdm


def main(data_root, dest_path):
    print('concat eval')
    print('Processing:', dest_path)
    print('data root:', data_root)
    print()

    if dest_path.split('/')[-1][:4] == 'test':
        map_path = os.path.join(dest_path, 'test.map')
    else:
        map_path = os.path.join(dest_path, 'enroll.map')

    utt2spk_path = os.path.join(dest_path, 'utt2spk')

    wav_save_root = os.path.join(dest_path, 'wav_concat')
    wav_scp_path = os.path.join(dest_path, 'wav_concat.scp')


    utt2spk = {}
    with open(utt2spk_path, 'r') as f:
        for line in f.readlines():
            line = line.strip().split()
            utt2spk[line[0]] = line[1]
    print('utt2spk:', len(utt2spk), 'utts')

    num = 0
    with open(map_path, 'r') as map_f, open(wav_scp_path, 'w') as wav_scp_f:
        for line in tqdm(map_f.readlines()):
            line = line.strip().split()
            utts_id = line[0]
            spk = utt2spk[utts_id]
            wav_save_path = os.path.join(wav_save_root, spk)
            os.makedirs(wav_save_path, exist_ok=True)
            fpath = os.path.join(wav_save_path, utts_id + '.wav')
            cmd = 'sox'
            for utt in line[1:]:
                cmd += ' ' + os.path.join(data_root, utt)
            cmd += ' -t wav -e signed-integer -b 16 -r 8000 {}'.format(fpath)
            state = os.system(cmd)
            if state == 0:
                num += 1
            else:
                raise SystemError(cmd)
            wav_scp_f.write('{} {}\n'.format(utts_id, fpath))

    print('Process OK. wav num:', num)


if __name__ == '__main__':
    fire.Fire(main)
