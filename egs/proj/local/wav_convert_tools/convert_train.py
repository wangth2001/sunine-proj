import os
from tqdm import tqdm

from split_channel import get_channel


work_path = r'/home/wangtianhao/coproj/230329-HUAWEI-Robust'

sph2pipe_scp_path = os.path.join(work_path, r'data/train/wav.scp')
utt2spk_path = os.path.join(work_path, r'data/train/utt2spk')

dest_scp_path = os.path.join(work_path, r'data_converted/train/wav.scp')
wav_save_path = os.path.join(work_path, r'data_converted/train/data')

if not os.path.exists(wav_save_path):
    os.makedirs(wav_save_path)


spks = set()
utt2spk = {}
with open(utt2spk_path, 'r') as f:
    for line in f.readlines():
        line = line.strip().split()
        utt2spk[line[0]] = line[1]
        spks.add(line[1])
print(len(spks), 'spks')
print(len(utt2spk), 'utts')

num = 0
with open(sph2pipe_scp_path, 'r') as sph_f, open(dest_scp_path, 'w') as wav_f:
    for line in tqdm(sph_f.readlines()):
        line = str(line)[:-1]
        pos = line.find(' ')
        utt = line[:pos]
        spk = utt2spk[utt]
        suffix = line[pos + 1:]
        save_path = os.path.join(wav_save_path, spk)
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        wav_file = os.path.join(save_path, utt + '.wav')
        data_type = suffix[-4:]
        if data_type == '.wav':
            # guowang multi channel data
            # get channel 0 (left channel)
            get_channel(suffix, 0, wav_file)
            num += 1
        else:
            # sph data
            # convert to wav file using sph2pipe
            cmd = suffix[:-2] + ' ' + wav_file
            state = os.system(cmd)
            if state == 0:
                num += 1
        wav_f.write("{} {}\n".format(utt, wav_file))

print('process num:', num)

if num == len(utt2spk):
    print('Process OK')
else:
    print('Process ERROR')
