#!/bin/bash

# Convert audios to 8kHz

work_path=/home/wangtianhao/coproj/230329-HUAWEI-Robust

vox1_wav_path=/ssd2/VoxCeleb/voxceleb1/voxceleb1_wav
voxceleb2_path=/ssd2/VoxCeleb/voxceleb2
cnceleb1_path=/ssd2/CNC_v2.0/CN-Celeb
cnceleb2_path=/ssd2/CNC_v2.0/CN-Celeb2

# convert voxceleb1
if [ $stage -le 1 ] && [ $stop_stage -ge 1 ];then
  for dest_path in $work_path/data_converted/eval/vox_metadata/vox1_1000/*; do
    echo $dest_path
    python convert_eval.py \
        --data_root $vox1_wav_path \
        --dest_path $dest_path \
        --save_root $work_path/data_converted/voxceleb1_8k
  done
fi

# convert voxceleb2
if [ $stage -le 2 ] && [ $stop_stage -ge 2 ];then
  voxceleb2_links=$work_path/data_converted/eval/voxceleb2_links
  [ -d $voxceleb2_links ] && rm -r $voxceleb2_links
  mkdir -p $voxceleb2_links

  for spk in `ls $voxceleb2_path/dev/aac`; do
    ln -s $voxceleb2_path/dev/aac/$spk $voxceleb2_links/$spk
  done

  for spk in `ls $voxceleb2_path/test/aac`; do
    ln -s $voxceleb2_path/test/aac/$spk $voxceleb2_links/$spk
  done

  for dest_path in $work_path/data_converted/eval/vox_metadata/vox2_6000/*; do
    echo $dest_path
    python convert_eval.py \
        --data_root $voxceleb2_links \
        --dest_path $dest_path \
        --save_root $work_path/data_converted/voxceleb2_8k
  done
fi

# make cnceleb1 and cnceleb2 links
if [ $stage -le 3 ] && [ $stop_stage -ge 3 ];then
  cnceleb_links=$work_path/data_converted/eval/cnceleb_links
  [ -d $cnceleb_links ] && rm -r $cnceleb_links
  mkdir -p $cnceleb_links

  for spk in `ls $cnceleb1_path/data`; do
    ln -s $cnceleb1_path/data/$spk $cnceleb_links/$spk
  done

  for spk in `ls $cnceleb2_path/data`; do
    ln -s $cnceleb2_path/data/$spk $cnceleb_links/$spk
  done

  # convert cnceleb
  for dest_path in $work_path/data_converted/eval/cnceleb/*/*; do
    echo $dest_path
    python convert_eval.py \
        --data_root $cnceleb_links \
        --dest_path $dest_path \
        --save_root $work_path/data_converted/cnceleb_8k
  done
fi

