import os
from tqdm import tqdm

# convert musan and rirs to 8kHz

musan_src_root = r'/ssd2/musan'
rirs_src_root = r'/ssd2/RIRS_NOISES'
work_root = r'/home/wangtianhao/coproj/230329-HUAWEI-Robust'

musan_dest_root = os.path.join(work_root, 'data_converted/musan_8k')
rirs_dest_root = os.path.join(work_root, 'data_converted/RIRS_NOISES_8k')

def main(src_root, dest_root):
    print('Processing ', src_root)
    print('output path:', dest_root)
    num_files = sum(len(files) for _, _, files in os.walk(src_root))
    print('file num:', num_files)

    wav_num = 0
    with tqdm(total=num_files) as pbar:
        for root, dirs, files in os.walk(src_root):
            for fname in files:
                output_root = str(root).replace(src_root, dest_root)
                if not os.path.exists(output_root):
                    os.makedirs(output_root)
                if fname.endswith('.wav'):
                    wav_src_path = os.path.join(root, fname)
                    wav_dest_path = os.path.join(output_root, fname)
                    cmd = 'sox {} -t wav -e signed-integer -b 16 -r 8000 {}'.format(wav_src_path, wav_dest_path)
                    state = os.system(cmd)
                    if state == 0:
                        wav_num += 1
                    else:
                        raise SystemError(cmd)
                pbar.update(1)
    print('Process OK. wav num:', wav_num)


if __name__ == '__main__':
    if os.path.exists(musan_src_root):
        main(musan_src_root, musan_dest_root)
    if os.path.exists(rirs_src_root):
        main(rirs_src_root, rirs_dest_root)
