import os
import fire
from tqdm import tqdm


def main(data_root, dest_path, save_root):
    print('Processing:', dest_path)
    print('data root:', data_root)
    print('save root:', save_root)
    print()

    if dest_path.split('/')[-1][:4] == 'test':
        map_path = os.path.join(dest_path, 'test.map')
    else:
        map_path = os.path.join(dest_path, 'enroll.map')

    wav_save_path = os.path.join(save_root, 'wav')

    wav_num = 0
    with open(map_path, 'r') as map_f:
        for line in tqdm(map_f.readlines()):
            line = line.strip().split()
            for utt in line[1:]:
                wav_src_path = os.path.join(data_root, utt)
                wav_dest_path = os.path.join(wav_save_path, utt)
                os.makedirs(os.path.dirname(wav_dest_path), exist_ok=True)
                cmd = 'sox {} -t wav -e signed-integer -b 16 -r 8000 {}'.format(wav_src_path, wav_dest_path)
                state = os.system(cmd)
                if state == 0:
                    wav_num += 1
                else:
                    raise SystemError(cmd)
    print('Process OK. wav num:', wav_num)


if __name__ == '__main__':
    fire.Fire(main)
