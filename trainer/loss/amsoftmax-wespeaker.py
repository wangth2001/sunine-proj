#! /usr/bin/python
# -*- encoding: utf-8 -*-
# Adapted from https://github.com/CoinCheung/pytorch-loss (MIT License)

import torch
import torch.nn as nn
import torch.nn.functional as F
try:
    from .utils import accuracy
except:
    from utils import accuracy

class LossFunction(nn.Module):
    def __init__(self, embedding_dim, num_classes, margin=0.2, scale=30, **kwargs):
        super(LossFunction, self).__init__()

        self.m = margin
        self.s = scale
        self.in_feats = embedding_dim
        self.weight = nn.Parameter(torch.FloatTensor(num_classes, embedding_dim))
        self.ce = nn.CrossEntropyLoss()
        nn.init.xavier_uniform_(self.weight)

        print('Initialised wespeaker AM-Softmax m=%.3f s=%.3f'%(self.m, self.s))

    def update(self, margin):
        self.m = margin

    def forward(self, x, label=None):
        assert len(x.shape) == 3
        label = label.repeat_interleave(x.shape[1])
        x = x.reshape(-1, self.in_feats)
        assert x.size()[0] == label.size()[0]
        assert x.size()[1] == self.in_feats

        # ---------------- cos(theta) & phi(theta) ---------------
        cosine = F.linear(F.normalize(x), F.normalize(self.weight))
        phi = cosine - self.m
        # ---------------- convert label to one-hot ---------------
        one_hot = x.new_zeros(cosine.size())
        one_hot.scatter_(1, label.view(-1, 1).long(), 1)
        output = (one_hot * phi) + ((1.0 - one_hot) * cosine)
        output *= self.s

        loss    = self.ce(output, label)
        prec1   = accuracy(output.detach(), label.detach(), topk=(1,))[0]
        return loss, prec1

