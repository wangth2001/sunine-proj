import os
import fire
import kaldiio
import numpy as np

from plda import PldaAnalyzer
from score import cosine_score, PLDA_score


def main(plda_dim, plda_xvec_scp_path, plda_utt2spk_path, eval_xvec_scp_path, trials_path, scores_path=os.devnull):
    print('load plda training data ...')
    utt2spk = {}
    with open(plda_utt2spk_path, 'r') as utt2spk_f:
        for line in utt2spk_f.readlines():
            line = line.strip().split()
            utt2spk[line[0]] = line[1]
    dev_labels = []
    dev_vectors = []
    for utt, embedding in kaldiio.load_scp_sequential(plda_xvec_scp_path):
        dev_labels.append(utt2spk[utt])
        dev_vectors.append(embedding)
    dev_labels = np.array(dev_labels)
    dev_vectors = np.array(dev_vectors)

    print('plda training ...')
    plda = PldaAnalyzer(n_components=plda_dim)
    plda.fit(dev_vectors, dev_labels, num_iter=10)

    print('load eval data ...')
    trials = np.loadtxt(trials_path, dtype=str)
    index_mapping = {}
    eval_vectors = []
    for utt, embedding in kaldiio.load_scp_sequential(eval_xvec_scp_path):
        index_mapping[utt] = len(eval_vectors)
        eval_vectors.append(embedding)
    eval_vectors = np.array(eval_vectors)

    os.makedirs(os.path.dirname(scores_path), exist_ok=True)

    print('Scoring ...')
    eer, th, mindcf_e, mindcf_h = cosine_score(trials, index_mapping, eval_vectors, scores=scores_path + '.cosine')
    print('Cosine EER: {:.3f}%  minDCF(0.01): {:.5f}  minDCF(0.001): {:.5f}'.format(eer * 100, mindcf_e, mindcf_h))

    eval_vectors_trans = plda.transform(eval_vectors)
    eer, th, mindcf_e, mindcf_h = PLDA_score(trials, index_mapping, eval_vectors_trans, plda, scores=scores_path + '.plda')
    print('PLDA EER: {:.3f}%  minDCF(0.01): {:.5f}  minDCF(0.001): {:.5f}'.format(eer * 100, mindcf_e, mindcf_h))



if __name__ == '__main__':
    fire.Fire(main)
